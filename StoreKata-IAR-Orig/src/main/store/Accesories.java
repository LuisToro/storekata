package store;

public class Accesories implements ProductInterface{

	@Override
	public float calculateDiscount(OrderItem item) {
	    float itemAmount = item.getProduct().getUnitPrice() * item.getQuantity();
		float booksDiscount = 0;
		if (itemAmount >= 100) {
			booksDiscount = itemAmount * 10 / 100;
		}
		return itemAmount - booksDiscount;
	}

}
