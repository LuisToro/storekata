package store;

public class Bikes implements ProductInterface{
	
	@Override
	public float calculateDiscount(OrderItem item) {
		float itemAmount = item.getProduct().getUnitPrice() * item.getQuantity();
		return itemAmount - itemAmount * 20/100;
	}
}
