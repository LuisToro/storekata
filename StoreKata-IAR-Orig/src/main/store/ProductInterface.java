package store;

public interface ProductInterface {
	float calculateDiscount(OrderItem item);
}
