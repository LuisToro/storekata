package store;

public class OrderItem {
	
	private Product product;
	private int quantity;

	/*
	 * Order Item Constructor
	 */
	public OrderItem(Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	
	public Product getProduct() {
		return product;
	}

	public int getQuantity() {
		return quantity;
	}

	public float calculateTotalForItem() {
		float totalDiscount=0;
		ProductInterface product = getProductToCalculate();
		totalDiscount = product.calculateDiscount(this);
		
		return totalDiscount;
	}
	
	public ProductInterface getProductToCalculate() {
		ProductInterface product = null;
		if (getProduct().getCategory() == ProductCategory.Accessories) {
			product = new Accesories();
		}
		if (getProduct().getCategory() == ProductCategory.Bikes) {
			product = new Bikes();
		}
		if (getProduct().getCategory() == ProductCategory.Cloathing) {
			product = new Cloathes();
		}
		return product;
	}
}
